<?php

/**
 * Get the cached changes to a given task handler.
 */
function delegator_page_get_page_cache($name) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_get('delegator_page', $name);
  if (!$cache) {
    $cache = delegator_page_load($name);
    $cache->locked = ctools_object_cache_test('delegator_page', $name);
  }

  return $cache;
}

/**
 * Store changes to a task handler in the object cache.
 */
function delegator_page_set_page_cache($name, $page) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_set('delegator_page', $name, $page);
}

/**
 * Remove an item from the object cache.
 */
function delegator_page_clear_page_cache($name) {
  ctools_include('object-cache');
  ctools_object_cache_clear('delegator_page', $name);
}

/**
 * Callback generated when the add page process is finished.
 */
function delegator_page_add_subtask_finish(&$form_state) {
  $page = &$form_state['page'];

  // Create a real object from the cache
  delegator_page_save($page);

  // Clear the cache
  delegator_page_clear_page_cache($form_state['cache name']);
}

/**
 * Callback generated when the 'next' button is clicked.
 *
 * All we do here is store the cache.
 */
function delegator_page_add_subtask_next(&$form_state) {
  // Update the cache with changes.
  delegator_page_set_page_cache($form_state['cache name'], $form_state['page']);
}

/**
 * Callback generated when the 'cancel' button is clicked.
 *
 * All we do here is clear the cache.
 */
function delegator_page_add_subtask_cancel(&$form_state) {
  // Update the cache with changes.
  delegator_page_clear_page_cache($form_state['cache name']);
}

/**
 * Store the values from the basic settings form.
 */
function delegator_page_form_basic_submit($form, &$form_state) {
  if (!isset($form_state['page']->pid) && empty($form_state['page']->import)) {
    $form_state['page']->name = $form_state['values']['name'];
  }

  $form_state['page']->admin_title = $form_state['values']['admin_title'];
  $form_state['page']->path = $form_state['values']['path'];

  return $form;
}